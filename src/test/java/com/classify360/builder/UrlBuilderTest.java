package com.classify360.builder;

import com.classify360.modeling.common.code.builder.UrlBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;

import java.util.List;

import static com.classify360.modeling.common.code.util.ModelingConstant.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UrlBuilderTest {

    static List<String> paths = null;
    static MultiValueMap<String, String> queryParameterMap = new LinkedMultiValueMap<>();

    @BeforeAll
    public static void init(){
        paths = UrlBuilder.buildPath(SOLR, "tenant", UPDATE);
        queryParameterMap.add(WT, JSON);
        queryParameterMap.add(COMMIT, "true");
    }

    @Test
    public void testBuildUrl(){
        UriComponents uriComponents = UrlBuilder.buildUrl(HTTP, "hostname", "8080",paths, queryParameterMap);
        assertTrue("http://hostname:8080/solr/tenant/update?wt=json&commit=true".equals(uriComponents.toUriString()));
    }

    @Test
    public void testBuildUrlWithNoPaths(){
        UriComponents uriComponents = UrlBuilder.buildUrl(HTTP, "hostname", "8080",null, queryParameterMap);
        assertTrue("http://hostname:8080?wt=json&commit=true".equals(uriComponents.toUriString()));
    }

    @Test
    public void testBuildUrlWithNoQueryParameterMap(){
        UriComponents uriComponents = UrlBuilder.buildUrl(HTTP, "hostname", "8080",paths, null);
        assertTrue("http://hostname:8080/solr/tenant/update".equals(uriComponents.toUriString()));
    }

    @Test
    public void testBuildUrlWithNoPort(){
        UriComponents uriComponents = UrlBuilder.buildUrl(HTTP, "hostname", null,paths, queryParameterMap);
        assertTrue("http://hostname/solr/tenant/update?wt=json&commit=true".equals(uriComponents.toUriString()));
    }


}