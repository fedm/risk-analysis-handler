package com.classify360.utils;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Optional;

import static com.classify360.modeling.common.code.util.HttpMethodUtil.buildResponseEntity;
import static com.classify360.modeling.common.code.util.ModelingConstant.*;
import static org.junit.jupiter.api.Assertions.*;

class HttpMethodUtilTest {
    static Optional<String> expectedMessage;

    @BeforeAll
    public static void init(){
        expectedMessage = Optional.of(YOU_HAVE_EXHAUSTED_YOUR_API_REQUEST_QUOTA);
    }

    @Test
    public void testBuildResponseEntity_ShouldReturnResponseEntityWithHeaderMap(){
        MultiValueMap<String, String> expectedHeader = new LinkedMultiValueMap<>();
        Optional<String> expectedHeaderKey = Optional.of(LIMIT_REFRESH_PERIOD);
        Optional<String> expectedHeaderValue = Optional.of(THIRTY);
        expectedHeader.add(expectedHeaderKey.get(), expectedHeaderValue.get());
        Optional<ResponseEntity<String>> responseEntity = buildResponseEntity(expectedHeaderKey, expectedHeaderValue, expectedMessage,
                                                                                null, HttpStatus.OK);
        assertResponseEntity(responseEntity);

    }


    @Test
    public void testBuildResponseEntity_ShouldReturnResponseEntityWithHttpHeaders(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(LIMIT_REFRESH_PERIOD, THIRTY);
        Optional<ResponseEntity<String>> responseEntity = buildResponseEntity(Optional.empty(), Optional.empty(), expectedMessage,
                                                                     httpHeaders, HttpStatus.OK);
        assertResponseEntity(responseEntity);

    }


    private void assertResponseEntity(Optional<ResponseEntity<String>> responseEntity) {
        assertTrue(responseEntity.get().getStatusCode() == HttpStatus.OK);
        assertTrue(responseEntity.get().getBody().equals(expectedMessage.get()));
        assertTrue(responseEntity.get().getHeaders().entrySet()
                .stream()
                .filter(entry -> entry.getKey().equals(LIMIT_REFRESH_PERIOD))
                .filter(entry -> entry.getValue().get(0).equals(THIRTY))
                .findFirst().isPresent());
    }

}