package com.classify360.utils;

import static com.classify360.modeling.common.code.util.ModelingConstant.*;

public class TestData {



    public static final String JSON_DOCUMENT_ONE = "{\n" +
            "\t\t\"id\": \"b324a977-9bdd-4282-a36d-1c87af1671f0\",\n" +
            "\t\t\"owner_id\": \"b324a977-9bdd-4282-a36d-1c87af1671f0\",\n" +
            "\t\t\"reference_sius\": \"/path/to/a/file/email.eml\",\n" +
            "\t\t\"path_sius\": \"/path/to/a/file/email.eml\",\n" +
            "\t\t\"title_siss\": \"Email subject\",\n" +
            "\t\t\"text\": \"Address at 2400 Virginia Ave, Washington, DC 11223 abcdefghijklmnopqurtuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890. ssn Ha HaHa MetaCharacters address: 2910 86th Street, Brooklyn, NY 11223 Here at 321-555-4321. 123.555.1234, NIE 35 River Drive South APT 1000, Jersey City, NJ 07310. 123*555*1234 SSN 800-555-1234 900-555-1234 WA Mr. Schafer Mr Smith Ms Davis Mrs. Robinson Mr. T 203 24-2344 203-24-2344\",\n" +
            "\t\t\"extension_siss\": \"eml\",\n" +
            "\t\t\"mime_sius\": \"text/plain\",\n" +
            "\t\t\"size_lius\": 512369,\n" +
            "\t\t\"attachment_bius\": false,\n" +
            "\t\t\"created_zius\": \"2020-06-11T13:30:07.400Z\",\n" +
            "\t\t\"accessed_zius\": \"2020-06-11T13:30:07.400Z\",\n" +
            "\t\t\"modified_zius\": \"2020-06-11T13:30:07.400Z\",\n" +
            "\t\t\"author_f_sism\": \"Bob McFakerson\",\n" +
            "\t\t\"source_lius\": 174,\n" +
            "\t\t\"root_iius\": 1,\n" +
            "\t\t\"version_iius\": 1,\n" +
            "\t\t\"cc_e_f_sium\": {\n" +
            "\t\t\"add\": [\n" +
            "\t\t\"somemail@whatever.com\"\n" +
            "\t\t]\n" +
            "\t\t},\n" +
            "\t\t\"r0_k0_iium\": [\n" +
            "\t\t41,\n" +
            "\t\t16,\n" +
            "\t\t1\n" +
            "\t\t],\n" +
            "\t\t\"r1_k0_iium\": [\n" +
            "\t\t16\n" +
            "\t\t],\n" +
            "\t\t\"r0_k1_iium\": [\n" +
            "\t\t13,\n" +
            "\t\t12\n" +
            "\t\t],\n" +
            "\t\t\"ssn_sium\": [\n" +
            "\t\t\"2effd16222e7231ca3e6b26c1352c5eb6b7f4e35e8b2bff732cf46ccc374aadf\"\n" +
            "\t\t],\n" +
            "\t\t\"address_sium\": [\n" +
            "\t\t\"3062ce50f961e8fcfe86c1ea4648ffd08eec8086e7a0f6e8ddc90ff6a2cfd2ff\"\n" +
            "\n" +
            "\t\t]\n" +
            "\t}";

    public static final String JSON_DOCUMENT_TWO = "{\n" +
            "\t\t\"id\": \"b324a977-9bdd-4282-a36d-1c87af1671f1\",\n" +
            "\t\t\"owner_id\": \"b324a977-9bdd-4282-a36d-1c87af1671f1\",\n" +
            "\t\t\"reference_sius\": \"/path/to/a/file/email.eml\",\n" +
            "\t\t\"path_sius\": \"/path/to/a/file/email.eml\",\n" +
            "\t\t\"title_siss\": \"Email subject\",\n" +
            "\t\t\"text\": \"SSN Address\",\n" +
            "\t\t\"extension_siss\": \"eml\",\n" +
            "\t\t\"mime_sius\": \"text/plain\",\n" +
            "\t\t\"size_lius\": 512369,\n" +
            "\t\t\"attachment_bius\": false,\n" +
            "\t\t\"created_zius\": \"2020-06-11T13:30:07.400Z\",\n" +
            "\t\t\"accessed_zius\": \"2020-06-11T13:30:07.400Z\",\n" +
            "\t\t\"modified_zius\": \"2020-06-11T13:30:07.400Z\",\n" +
            "\t\t\"author_f_sism\": \"Bob McFakerson\",\n" +
            "\t\t\"source_lius\": 174,\n" +
            "\t\t\"root_iius\": 1,\n" +
            "\t\t\"version_iius\": 1,\n" +
            "\t\t\"cc_e_f_sium\": {\n" +
            "\t\t\"add\": [\n" +
            "\t\t\"somemail@whatever.com\"\n" +
            "\t\t]\n" +
            "\t\t},\n" +
            "\t\t\"r0_k0_iium\": [\n" +
            "\t\t41,\n" +
            "\t\t16,\n" +
            "\t\t1\n" +
            "\t\t],\n" +
            "\t\t\"r1_k0_iium\": [\n" +
            "\t\t16\n" +
            "\t\t],\n" +
            "\t\t\"r0_k1_iium\": [\n" +
            "\t\t13,\n" +
            "\t\t12\n" +
            "\t\t],\n" +
            "\t\t\"ssn_sium\": [\n" +
            "\t\t\"2effd16222e7231ca3e6b26c1352c5eb6b7f4e35e8b2bff732cf46ccc374aadf\"\n" +
            "\t\t],\n" +
            "\t\t\"address_sium\": [\n" +
            "\t\t\"3062ce50f961e8fcfe86c1ea4648ffd08eec8086e7a0f6e8ddc90ff6a2cfd2ff\"\n" +
            "\n" +
            "\t\t]\n" +
            "\t}";

    public static final String JSON_DOCUMENTS = OPEN_SQUARE_BRACKET.concat(JSON_DOCUMENT_ONE).concat(COMMA).
                                                concat(JSON_DOCUMENT_TWO).concat(CLOSED_SQUARE_BRACKET);

    public static final String JSON_ONE = "{\"id\":\"123\",\"score\":0.8}";
    public static final String JSON_TWO = "{\"id\":\"456\",\"score\":0.2}";
    public static final String JSONS = OPEN_SQUARE_BRACKET.concat(JSON_ONE).concat(COMMA).
                                        concat(JSON_TWO).concat(CLOSED_SQUARE_BRACKET);
    public static final String MODEL_POINT_ONE = "{\"r\":0,\"k\":0,\"p\":41,\"s\":0.8}";
    public static final String MODEL_POINT_TWO = "{\"r\":1,\"k\":0,\"p\":20,\"s\":0.5}";
    public static final String MODEL_POINTS = OPEN_SQUARE_BRACKET.concat(MODEL_POINT_ONE).concat(COMMA).
                                                concat(MODEL_POINT_TWO).concat(CLOSED_SQUARE_BRACKET);
}
