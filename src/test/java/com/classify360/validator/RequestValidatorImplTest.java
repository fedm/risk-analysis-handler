package com.classify360.validator;

import com.classify360.modeling.common.code.validator.RequestValidator;
import com.classify360.modeling.common.code.validator.RequestValidatorImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static com.classify360.modeling.common.code.util.HttpMethodUtil.buildResponseEntity;
import static com.classify360.modeling.common.code.util.ModelingConstant.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RequestValidatorImplTest {

    private static RequestValidator requestValidator;

    @BeforeAll
    public static void init(){
         requestValidator = new RequestValidatorImpl();
    }

    @Test
    public void testTenant_WithTenant_ShouldNotReturnResponseEntity(){
        assertTrue(requestValidator.tenant(Optional.of(TENANT)).isEmpty());
    }

    @Test
    public void testTenant_WithNoTenant_ShouldReturnResponseEntity(){
        Optional<ResponseEntity<String>> expectedResponse = buildResponseEntity(Optional.empty(), Optional.empty(),Optional.of(SUPPLY_TENANT), null, HttpStatus.BAD_REQUEST);
        Optional<ResponseEntity<String>> actualResponse = requestValidator.tenant(Optional.empty());
        assertTrue(expectedResponse.equals(actualResponse));
    }

    @Test
    public void testValidate_WithValidParameters_ShouldNotReturnResponseEntity(){
        assertTrue(requestValidator.validate(Optional.of(TENANT), Optional.of(Boolean.TRUE)).isEmpty());
    }

    @Test
    public void testValidate_WithEmptyCommitParameter_ShouldReturnResponseEntity(){
        Optional<ResponseEntity<String>> expectedResponse = buildResponseEntity(Optional.empty(), Optional.empty(), Optional.of(SUPPLY_COMMIT), null, HttpStatus.BAD_REQUEST);
        Optional<ResponseEntity<String>> actualResponse = requestValidator.validate(Optional.of(TENANT), Optional.empty());
        assertTrue(expectedResponse.equals(actualResponse));
    }


}