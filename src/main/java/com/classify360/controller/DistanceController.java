package com.classify360.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.classify360.modeling.common.code.util.HttpMethodUtil.buildResponseEntity;
import static com.classify360.modeling.common.code.util.JsonUtil.getJson;
import static com.classify360.modeling.common.code.util.JsonUtil.getJsonObject;
import static com.classify360.modeling.common.code.util.ModelingConstant.FEATURE_EXTRACTION;
import static com.classify360.modeling.common.code.util.ModelingConstant.RISK_ANALYSIS_SERVICE_BISS;


@RestController
public class DistanceController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(DistanceController.class);

    @PostMapping(FEATURE_EXTRACTION)
    @ResponseStatus(HttpStatus.OK)
    @RateLimiter(name="RiskAnalysisHandlerControllerRateLimiter", fallbackMethod = "rateLimitPermitExpired")
    public ResponseEntity<String> getDocumentsForFeatureExtraction(@RequestBody String documents, @RequestParam Optional<String> tenant,
                                                                   @RequestParam Optional<Boolean> commit){
        Optional<ResponseEntity<String>> responseEntity = requestValidator.validate(tenant, commit);
        JsonArray jsonArray = null;
        try{
            jsonArray = getJson(documents);
        }catch (IllegalStateException exception){
            responseEntity = buildResponseEntity(Optional.empty(), Optional.empty(),Optional.of(exception.getMessage()), null,HttpStatus.BAD_REQUEST);
        }
        if(responseEntity.isEmpty()) {
            log.debug(String.format("Tenant %s, Commit %s", tenant.get(), commit.get()));
            List<Pair<JsonObject,Optional<ResponseEntity<String>>>> responses = invokeAll(jsonArray);
            Map<Boolean, List<Pair<JsonObject, Optional<ResponseEntity<String>>>>> partitionedResponseEntities = partitionResponseEntities(responses);
            responseEntity = getResponseEntityFuture(tenant, commit, partitionedResponseEntities);
        }
        log.info("Finished processing the batch.");
        return  responseEntity.get();
    }

    private Map<Boolean, List<Pair<JsonObject, Optional<ResponseEntity<String>>>>> partitionResponseEntities(List<Pair<JsonObject, Optional<ResponseEntity<String>>>> responses) {
        return responses.stream()
                .collect(Collectors.partitioningBy(jsonObjectResponseEntityPair -> jsonObjectResponseEntityPair.getRight()
                                                                                                .get().getStatusCode().isError()));
    }

    private Optional<ResponseEntity<String>> getResponseEntityFuture(Optional<String> tenant, Optional<Boolean> commit, Map<Boolean, List<Pair<JsonObject, Optional<ResponseEntity<String>>>>> partitionResponseEntities) {
        JsonArray responseBodies = getBodiesFuture(partitionResponseEntities);
        Optional<ResponseEntity<String>> responseEntity = solrService.post(responseBodies.toString(), tenant.get(), commit.get());
        return responseEntity;
    }


    private JsonArray getBodiesFuture(Map<Boolean, List<Pair<JsonObject, Optional<ResponseEntity<String>>>>>  partitionResponseEntities) {
        JsonArray jsonArray = new JsonArray();
        partitionResponseEntities.entrySet()
                                    .stream()
                                    .forEach( entry -> {
                                        if(entry.getKey()){
                                            entry.getValue().stream().forEach(
                                                   errorPair -> {
                                                       JsonObject erroneous = errorPair.getLeft();
                                                       erroneous.addProperty(RISK_ANALYSIS_SERVICE_BISS, false);
                                                       log.debug("Erroneous source document " + erroneous );
                                                       log.debug("Erroneous processed response " + errorPair.getRight() );
                                                       jsonArray.add(erroneous);
                                                   }
                                            );
                                        }
                                        else {
                                            entry.getValue().stream().forEach(
                                                    jsonObjectResponsePair -> {
                                                        JsonObject jsonObject = getJsonObject(jsonObjectResponsePair.getRight().get().getBody());
                                                        jsonArray.add(jsonObject);
                                                    }
                                            );
                                        }
                                    });
        return jsonArray;
    }

    public ResponseEntity<String> rateLimitPermitExpired(String documents, Optional<String> tenant, Optional<Boolean> commit, Exception exception) {
       return limitExhausted().get();
    }



}
