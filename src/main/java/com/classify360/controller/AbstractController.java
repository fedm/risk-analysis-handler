package com.classify360.controller;

import com.classify360.modeling.common.code.service.SolrService;
import com.classify360.modeling.common.code.validator.RequestValidator;
import com.classify360.services.DictionaryService;
import com.classify360.services.FeatureExtractionService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.classify360.modeling.common.code.util.HttpMethodUtil.buildResponseEntity;
import static com.classify360.modeling.common.code.util.ModelingConstant.*;

public abstract class AbstractController {

    private static final Logger log = LoggerFactory.getLogger(AbstractController.class);

    @Value("${risk.analysis.service.indexNameDictionaryBoolean}")
    private List<String> indexNameDictionaryBoolean;

    @Value("${risk.analysis.service.indexNameDictionaryMatchCounter}")
    private List<String> indexNameDictionaryMatchCounter;

    @Value("${risk.analysis.service.indexNameDictionaryNameSurname}")
    private List<String> indexNameDictionaryNameSurname;

    @Value("${risk.analysis.service.indexSurnameDictionaryNameSurname}")
    private String indexSurnameDictionaryNameSurname;

    @Value("${risk.analysis.service.includeDictionaryMatchCounter}")
    private Boolean includeDictionaryMatchCounter;

    @Value("${risk.analysis.service.includeDictionaryBoolean}")
    private Boolean includeDictionaryBoolean;

    @Value("${risk.analysis.service.includeDictionaryNameSurname}")
    private Boolean includeDictionaryNameSurname;

    @Autowired
    SolrService solrService;

    @Autowired
    RequestValidator requestValidator;

    @Autowired
    FeatureExtractionService featureExtractionService;

    @Autowired
    @Qualifier("dictionaryBooleanImpl")
    DictionaryService dictionaryBooleanImpl;

    @Autowired
    @Qualifier("dictionaryMatchCounterImpl")
    DictionaryService dictionaryMatchCounterImpl;

    @Autowired
    @Qualifier("dictionaryNameSurnameImpl")
    DictionaryService dictionaryNameSurnameImpl;

    @Value("${resilience4j.ratelimiter.instances.RiskAnalysisHandlerControllerRateLimiter.limitRefreshPeriod}")
    protected String rateLimiterLimitRefreshPeriod;

    @Value("${risk.analysis.feature.extractor.unGuidedModeling}")
    private Boolean unGuidedModeling;

    @Value("${risk.analysis.feature.extractor.noOfInstances}")
    private String noOfInstances;

    protected List<Pair<JsonObject,Optional<ResponseEntity<String>>>>  invokeAll(JsonArray jsonArray){
        ExecutorService executorService =
                Executors.newFixedThreadPool(noOfInstances.equals(ZERO) ? jsonArray.size() + TWO : Integer.valueOf(noOfInstances));
        List<CompletableFuture<Pair<JsonObject,Optional<ResponseEntity<String>>>>> wrappedCalculatedDistance =
                                                                getAllDistances(jsonArray, executorService);
        List<Pair<JsonObject,Optional<ResponseEntity<String>>>> calculatedDistance = wrappedCalculatedDistance.stream()
                                                                            .map(CompletableFuture::join)
                                                                            .collect(Collectors.toList());
        executorService.shutdown();
        return calculatedDistance;
    }

    private List<CompletableFuture<Pair<JsonObject,Optional<ResponseEntity<String>>>>> getAllDistances(JsonArray jsonArray, ExecutorService executorService) {
        List<CompletableFuture<Pair<JsonObject,Optional<ResponseEntity<String>>>>> result =  StreamSupport.stream(jsonArray.spliterator(), false)
                        .map(document -> (JsonObject) document)
                        .map(document -> {
                            log.debug("Will process id " + ((JsonPrimitive)document.get(ID)).getAsString());
                            log.debug("Will process the document " + document);
                            if((JsonPrimitive)document.get(TEXT)!=null) {
                                log.debug("Size of incoming document in bytes --> " + ((JsonPrimitive) document.get(TEXT)).getAsString().length() * 2);
                            }
                            return document;
                        })
                        .map(document -> getDistance(document, executorService))
                        .collect(Collectors.toList());
        return result;
    }

    private CompletableFuture<Pair<JsonObject,Optional<ResponseEntity<String>>>> getDistance(JsonObject jsonObject, ExecutorService executorService) {
        CompletableFuture<Pair<JsonObject, Optional<ResponseEntity<String>>>>  distanceTask = CompletableFuture
                .supplyAsync(() ->
                        apply(jsonObject,  dictionaryBooleanImpl, indexNameDictionaryBoolean, null, includeDictionaryBoolean), executorService)
                .thenApply(pair -> apply(jsonObject, pair, dictionaryMatchCounterImpl,indexNameDictionaryMatchCounter, null, includeDictionaryMatchCounter))
                .thenApply(pair -> apply(jsonObject, pair, dictionaryNameSurnameImpl,indexNameDictionaryNameSurname, indexSurnameDictionaryNameSurname, includeDictionaryNameSurname))
                .thenApply(pair -> applyFeatureExtractor(jsonObject, pair))
                .handle((pair, exception) -> {
                    if (exception != null) {
                        log.error("Exception occurred while calling  services ", exception);
                        pair.setValue(buildResponseEntity(Optional.empty(), Optional.empty(), Optional.of(exception.getMessage()), null, HttpStatus.BAD_REQUEST));
                    }
                    return pair;
                });
        return distanceTask;
    }

    private Pair<JsonObject, Optional<ResponseEntity<String>>> apply(JsonObject jsonObject, DictionaryService dictionaryService,
                                                                        List<String> indexNames, String indexSurname, Boolean include) {
        Pair<JsonObject, Optional<ResponseEntity<String>>> pair = Pair.of(null, Optional.empty());
        if (include) {
            pair = process(jsonObject, null, dictionaryBooleanImpl, indexNameDictionaryBoolean, null);
        }else{
            pair = Pair.of(jsonObject, buildResponseEntity(Optional.empty(), Optional.empty(), Optional.of(jsonObject.toString()),
                                    null, HttpStatus.OK));
        }
        return pair;
    }

    private Pair<JsonObject, Optional<ResponseEntity<String>>> applyFeatureExtractor(JsonObject jsonObject, Pair<JsonObject, Optional<ResponseEntity<String>>> pair) {
        if(unGuidedModeling && !pair.getRight().get().getStatusCode().isError()) {
            Optional<ResponseEntity<String>> responseEntity = featureExtractionService.extract(pair.getRight().get().getBody());
            pair = Pair.of(jsonObject, responseEntity);
        }
        return pair;
    }

    private Pair<JsonObject, Optional<ResponseEntity<String>>> apply(JsonObject jsonObject, Pair<JsonObject, Optional<ResponseEntity<String>>> pair,
                                                                     DictionaryService dictionaryService, List<String> indexNames, String indexSurname, Boolean include) {
        if (include && !pair.getRight().get().getStatusCode().isError()) {
            pair = process(jsonObject, pair.getRight().get().getBody(), dictionaryService, indexNames, indexSurname);
        }
        return pair;
    }

    private Pair<JsonObject, Optional<ResponseEntity<String>>> process(JsonObject jsonObject, String payload, DictionaryService dictionaryService, List<String> indexNames, String indexSurname) {
        Pair<JsonObject, Optional<ResponseEntity<String>>> result = Pair.of(null, Optional.empty());
        payload = payload==null? jsonObject.toString():payload;
        Iterator<String> indexName = indexNames.iterator();
        do {
            Optional<ResponseEntity<String>> responseEntity = dictionaryService.process(payload, indexName.next(), indexSurname);
            payload = responseEntity.get().getBody();
            result = Pair.of(jsonObject, responseEntity);
        }while(!result.getRight().get().getStatusCode().isError() && indexName.hasNext());

        return result;
    }


    protected Optional<ResponseEntity<String>> limitExhausted() {
        log.info(YOU_HAVE_EXHAUSTED_YOUR_API_REQUEST_QUOTA);
        return buildResponseEntity(Optional.of(LIMIT_REFRESH_PERIOD), Optional.of(rateLimiterLimitRefreshPeriod),
                Optional.of(YOU_HAVE_EXHAUSTED_YOUR_API_REQUEST_QUOTA), null,HttpStatus.TOO_MANY_REQUESTS );
    }


}
