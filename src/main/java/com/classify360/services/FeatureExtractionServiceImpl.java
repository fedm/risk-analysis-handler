package com.classify360.services;


import com.classify360.modeling.common.code.builder.UrlBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.classify360.modeling.common.code.util.HttpMethodUtil.request;
import static com.classify360.modeling.common.code.util.ModelingConstant.*;

@Service
public class FeatureExtractionServiceImpl extends AbstractServiceImpl implements FeatureExtractionService{

    private static final Logger log = LoggerFactory.getLogger(FeatureExtractionServiceImpl.class);

    @Value("${risk.analysis.feature.extractor.hostName}")
    private String riskAnalysisFeatureExtractorHostName;

    @Value("${risk.analysis.feature.extractor.port}")
    private String port;

    @Value("${risk.analysis.feature.extractor.endPoint}")
    private String endPoint;

    @Override
    public Optional<ResponseEntity<String>> extract(String dataDocuments) {
        String url = UrlBuilder.buildUrl(HTTP, riskAnalysisFeatureExtractorHostName, port, UrlBuilder.buildPath(endPoint)).toUriString();
        Optional<ResponseEntity<String>> responseEntity =
                request(restTemplate, HttpMethod.POST, dataDocuments, null, url);
        log.debug("Received for feature extraction " + responseEntity.get().getBody());
        return responseEntity;

    }
}
