package com.classify360.services;

import com.classify360.modeling.common.code.builder.UrlBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Optional;

import static com.classify360.modeling.common.code.util.HttpMethodUtil.request;
import static com.classify360.modeling.common.code.util.ModelingConstant.*;

@Service
public class DictionaryNameSurnameImpl extends AbstractServiceImpl implements DictionaryService{

    private static final Logger log = LoggerFactory.getLogger(DictionaryNameSurnameImpl.class);

    @Value("${risk.analysis.service.dictionaryNameSurname}")
    private String dictionaryNameSurname;

    @Override
    public Optional<ResponseEntity<String>> process(String dataDocuments, String indexName, String indexSurname) {
        MultiValueMap<String, String> queryParameterMap = new LinkedMultiValueMap<>();
        queryParameterMap.add(INDEX_NAME, indexName);
        queryParameterMap.add(INDEX_SURNAME, indexSurname);
        String url = UrlBuilder.buildUrl(HTTP, riskAnalysisServiceHostName, port, UrlBuilder.buildPath(dictionaryNameSurname), queryParameterMap).toUriString();
        Optional<ResponseEntity<String>> responseEntity =
                request(restTemplate, HttpMethod.POST, dataDocuments, null, url);
        log.debug("Received for dictionary name surname " + responseEntity.get().getBody());
        return responseEntity;
    }
}
