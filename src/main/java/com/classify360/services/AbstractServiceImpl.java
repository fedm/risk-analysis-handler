package com.classify360.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

public abstract class AbstractServiceImpl {

    @Value("${risk.analysis.service.hostName}")
    protected String riskAnalysisServiceHostName;

    @Value("${risk.analysis.service.port}")
    protected String port;

    @Autowired
    protected RestTemplate restTemplate;

}
