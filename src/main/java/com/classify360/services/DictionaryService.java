package com.classify360.services;

import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface DictionaryService {

    Optional<ResponseEntity<String>> process(String dataDocuments, String indexName, String indexSurname);
}
