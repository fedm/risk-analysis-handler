package com.classify360.services;

import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface FeatureExtractionService {

    Optional<ResponseEntity<String>> extract(String dataDocuments);
}
