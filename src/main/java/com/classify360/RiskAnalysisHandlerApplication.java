package com.classify360;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;


@SpringBootApplication
public class RiskAnalysisHandlerApplication  {

	@Value("${api.connectTimeout}")
	private String apiConnectTimeout;

	@Value("${api.readTimeout}")
	private String apiReadTimeout;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.setConnectTimeout(Duration.ofSeconds(Integer.valueOf(apiConnectTimeout)))
						.setReadTimeout(Duration.ofSeconds(Integer.valueOf(apiReadTimeout)))
						.build();
	}

	public static void main(String[] args) {
		SpringApplication.run(RiskAnalysisHandlerApplication.class, args);
	}


}
