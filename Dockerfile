#docker run --link service-registry-service:service-registry-service --name=risk-analysis-handler risk-analysis-handler -p 80:8080 -Xms12g -Xmx12g -XX:ActiveProcessorCount=8 -Deureka.client.serviceUrl.defaultZone=http://service-registry-service:8761/eureka/ -verbose:gc -XX:+PrintGCDetails -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=heapdumps -jar risk-analysis-handler-0.0.1-SNAPSHOT.jar
FROM amazonlinux
RUN yum -y install sudo
RUN amazon-linux-extras enable java-openjdk11
RUN yum install java-11-openjdk-headless -y
RUN sudo mkdir /opt/risk-analysis-handler
COPY  target/risk-analysis-handler-0.0.1-SNAPSHOT.jar /opt/risk-analysis-handler
WORKDIR  /opt/risk-analysis-handler
ENTRYPOINT ["java"]

